import {BrowserRouter, Route, Switch} from "react-router-dom";
import Home from "./containers/Home/Home";
import React from "react";
import Layout from "./components/Navigation/Layout/Layout";
import Edit from "./components/Edit/Edit";

function App() {
    return (
        <BrowserRouter>
            <Layout/>
                <Switch>
                    <Route path='/' exact component={Home}/>
                    <Route path='/pages/about' component={Home}/>
                    <Route path='/pages/contacts' component={Home}/>
                    <Route path='/pages/services' component={Home}/>
                    <Route path='/pages/messages' component={Home}/>
                    <Route path='/pages/articles' component={Home}/>
                    <Route path='/pages/titles' component={Home}/>
                    <Route path='/pages/news' component={Home}/>
                    <Route path='/pages/admin' component={Edit}/>
                </Switch>
        </BrowserRouter>
    );
}

export default App;
