import React from 'react';

const Navigations = ({c}) => {
    if (Math.random() > 0.7) throw new Error('Well, This happened')
    return (
            <div className='mt-4'>
                <h2>{c.title}</h2>
                <p>{c.content}</p>
            </div>
    );
};

export default Navigations;