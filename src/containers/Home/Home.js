import React, {useEffect, useState} from 'react';
import {axiosApi} from "../../axiosApi";
import Spinner from "../../components/Spinner/Spinner";
import Navigations from "../Navigations/Navigations";
import ErrorBoundary from "../../components/ErrorBoundary/ErrorBoundary";


const Home = ({match}) => {
    const [content, setContent] = useState([]);
    const [loading, setLoading] = useState(false);

    axiosApi.interceptors.request.use(req => {
        setLoading(true)
        return req;
    });

    axiosApi.interceptors.response.use(res => {
        setLoading(false)
        return res;
    }, err => {
        setLoading(false)
        throw err;
    });

    useEffect(() => {
        const fetchData = async () => {
            if (match.path === '/') {
                const response = await axiosApi.get('/pages.json');
                const fetchedOrders = Object.keys(response.data).map(id => {
                    const order = response.data[id];
                    return {...order};
                });
                setContent(fetchedOrders)
            } else {
                const response = await axiosApi.get(match.path + '.json');
                setContent([response.data]);
            }
        }

        fetchData().finally(console.error)
    }, [match.path]);

    const contents = content.map(c =>
            <ErrorBoundary key={c.title}><Navigations c={c}/></ErrorBoundary>
    );

    return (
        loading ? <Spinner/> : <div className="container mt-4 pt-4">
            {contents}
        </div>
    );
};

export default Home